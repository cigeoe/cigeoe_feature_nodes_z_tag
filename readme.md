Feature nodes Z tag is a plugin for QGis.

The purpose of this plugin is to show the Z coordinate of each geometry vertex of a feature.

Usage:
- press the plugin button to open its interface;
- you can set the size of the Z label, color and numer of digits to show as decimal places;
![ALT](/images/image1.jpg)
- when you select a feature, a tag with the corresponding Z value will be shown in each vertex.
![ALT](/images/image2.jpg)


Centro de Informação Geoespacial do Exército (www.igeoe.pt)

Portugal
